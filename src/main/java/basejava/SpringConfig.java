package basejava;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SpringConfig {
	@Bean
	public Performer duke() {
		return new Juggler();
	}

	@Bean
	public Performer duke15() {
		return new Juggler(15);
	}
}
