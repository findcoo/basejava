package basejava;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {
	public static void main(String args[]) {
		ApplicationContext atx = new ClassPathXmlApplicationContext("basejava.xml");	
		Performer performer = (Performer)atx.getBean("duke");
		Performer performer2 = (Performer)atx.getBean("duke15");

		performer.perform(); 
		performer2.perform();
	}
}
