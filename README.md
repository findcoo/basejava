## Java 기반 bean 설정

1. **디렉토리 구성**
```
./src/
└── main
    ├── java
    │   └── basejava
    │       ├── Juggler.java
    │       ├── Main.java
    │       ├── Performer.java
    │       └── SpringConfig.java
    └── resources
        └── basejava.xml

```
---
2. **주의 및 참고**
	+ `@Configuration` 어노테이션을 이용해 자바 기반 bean 설정 클래스를 정의한다.
  + 메소드에 `@Bean` 어노테이션을 적용하여 메소드가 bean으로 생성되게 설정한다.
---
